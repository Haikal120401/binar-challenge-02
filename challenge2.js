const checkTypeNumber = (givenNumber)=>{
    if(typeof givenNumber === 'number'){
        if (givenNumber % 2 == 0 ){
            console.log("GENAP")
        }else {
            console.log("GANJIL")
        }
    }else if(givenNumber == null){
        console.log("where is the parameters?")
    }else {
        console.log("Error : Invalid data type")
    }
}

console.log(checkTypeNumber(12))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())